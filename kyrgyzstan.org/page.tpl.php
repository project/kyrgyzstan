<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

  <head>
    <title><?php print $head_title ?></title>
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <?php print $head ?>
    <?php print $styles ?>
  </head>
  
<body <?php print theme("onload_attribute"); ?>>
	<div class="main0">
	<div class="main">
		<div class="header">
					<div class="top_info">
					<div class="top_info_left">
					<a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="http://www.kyrgyzstan.org/files/logo.gif"></a></div>
				<div class="top_info_right">
          <?// php print kyrgyzstan_login_box(); ?>
		 Embassy of Kyrgyz Republic to USA:<br>
<a href="http://www.kyrgyzembassy.org">www.kyrgyzembassy.org</a>
				</div>		
				    </div>
		</div>
		
		<div class="bar">
			<ul>
        <?php if ($site_slogan) { ?><li class="slogan"><?php print $site_slogan ?></li><?php } ?>	 
			</ul>
      <?php print kyrgyzstan_primary_links(); ?>
		</div>

     <?php if ($mission != ""): ?>
      <div class="subheader"> 
      <?php print $mission; ?>
      </div> 
    <?php endif; ?> 

	<div class="left<?php if (kyrgyzstan_is_admin()) print ' left_admin_view';?>">
	<img src="http://www.kyrgyzstan.org/themes/kyrgyzstan/logo/<?include'http://www.kyrgyzstan.org/themes/kyrgyzstan/rand.php';?>">
		<br><br>		
	
		<div class="left_articles">
        <?php print $breadcrumb ?>
        <?php if ($title) {?><h2 class="title"><?php print $title ?></h2> <?php };?>
        <?php if ($tabs) {?><div class="tabs"><?php print $tabs ?></div> <?php };?>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
		</div>
	</div>
<div class=middle><script type="text/javascript"><!--
google_ad_client = "pub-8613243344719809";
google_ad_width = 120;
google_ad_height = 600;
google_ad_format = "120x600_as";
google_ad_type = "text_image";
google_ad_channel ="5018314259";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></div>


    <?php if (!kyrgyzstan_is_admin()) { ?>
  		<div class="right">

        <?php if ($search_box): ?>
    			<div class="right_login">
            <?php print $search_box; ?>
    			</div>
        <?php endif; ?> 

  			<div class="right_articles">
         <?php print $sidebar_left ?>
         <?php print $sidebar_right ?>
  			</div>
  		</div>	
    <?php } else {?> 
  		<div class="right_admin">
  		  <h2>Navigation</h2>
        <?php print theme('menu_tree');  ?>
  		</div>	
    <?php }; ?> 

		<div class="footer">
		    <?php print kyrgyzstan_secondary_links(); ?>
      <?php print $footer_message ?>
      <?php print $closure ?>
		</div>
	</div>
	
	</div>
</body>
</html>

