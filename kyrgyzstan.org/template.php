<?php
function kyrgyzstan_regions() {
  return array(
       'right' => t('right sidebar'),
       'content' => t('content'),
       'header' => t('header'),
       'footer' => t('footer')
  );
}
function kyrgyzstan_login_box() {
  global $user;
 
  if (!$user->uid) {
    //$output = '<p><b>You are not Logged in!</b> <a href="#">Log in</a> to check your messages.<br />';
    //$output .= 'Do you want to <a href="#">Log in</a> or <a href="#">register</a>?</p>';
    $form['#action'] = url($_GET['q'], drupal_get_destination());
    $form['#id'] = 'user-login-form';
    $form['name'] = array('#type' => 'textfield',
      '#title' => t('Username'),
      '#maxlength' => 60,
      '#size' => 15,
      '#required' => TRUE,
    );
    $form['pass'] = array('#type' => 'password',
      '#title' => t('Password'),
      '#size' => 15,
      '#required' => TRUE,
    );
    $form['submit'] = array('#type' => 'submit',
      '#value' => t('Log in'),
    );

    if (variable_get('user_register', 1)) {
      $items[] = l(t('Create new account'), 'user/register', array('title' => t('Create a new user account.')));
    }
    $items[] = l(t('Request new password'), 'user/password', array('title' => t('Request new password via e-mail.')));
    $form['links'] = array('#value' => theme('item_list', $items));

    $output .= drupal_get_form('user_login_block', $form, 'user_login');

    return $output;
  }
  else {
    $output = '<p>You are logged in as <b>' . $user->name . '</b>';
    $output .= '&nbsp;&nbsp;&ndash;&nbsp;&nbsp;' . l(t('Logout'), 'logout', array('title' => t('Logout'))) . '<br>';
    $output .= l(t('Manage my account'), 'user/' . $user->uid . '', array('title' => t('manage my account')));
    $output .= '</p>';
    return $output;
  }
}
function kyrgyzstan_primary_links() {
  $links = menu_primary_links();
  if ($links) {
    $output .= '<ul>';
    foreach ($links as $link) {
      $output .= '<li>' . $link . '</li>';
    }; 
    $output .= '</ul>';
  }
  return $output;
}

function kyrgyzstan_secondary_links() {
  $links = menu_secondary_links();
  if ($links) {
  $output .= '<p>';
    foreach ($links as $link) { $output .= $link . ' | '; }; 
              }
	$output .= '<a href=/sitemenu>Sitemap</a></p>';
  return $output;
}

/* Checks to see if we're in the admin section */
function kyrgyzstan_is_admin() {
  if (((arg(0) == 'admin') || (arg(0) == 'administer') || (arg(0) == 'user') ) && (user_access('access administration pages'))) {
    return true;
  }
}

function phptemplate_menu_tree($pid = 1) {
  if ($tree = menu_tree($pid)) {
    $output .= "\n<ul>\n"; 
    if (function_exists('base_path')) {
      $base_path = base_path();
    }
    $url = $base_path;    
    if ($pid == 1) {
      $output .= "<li><a href=\"" . $url . "\">home</a></li>";
    };
    $output .= $tree; 
    $output .= "\n</ul>\n"; 
    return $output;
  }
}
function phptemplate_menu_item($mid, $children = '', $leaf = TRUE) {
  //return '<li class="'. ($leaf ? 'leaf' : ($children ? 'expanded' : 'collapsed')) .'">'. menu_item_link($mid) . $children ."</li>\n";
  return '<li>'. menu_item_link($mid) . $children ."</li>\n";
}
function phptemplate_system_themes($form) {
  foreach (element_children($form) as $key) {
    $row = array();
    if (is_array($form[$key]['description'])) {
      $row[] = form_render($form[$key]['description']) . form_render($form[$key]['screenshot']) ;
      $row[] = array('data' => form_render($form['status'][$key]), 'align' => 'center');
      if ($form['theme_default']) {
        $row[] = array('data' => form_render($form['theme_default'][$key]) . form_render($form[$key]['operations']), 'align' => 'center');
      }
    }
    $rows[] = $row;
  }
  $header = array(t('Name/Screenshot'), t('Enabled'), t('Default'));
  $output = theme('table', $header, $rows);
  $output .= form_render($form);
  return $output;
}
function phptemplate_system_user($form) {
  foreach (element_children($form) as $key) {
    $row = array();
    if (is_array($form[$key]['description'])) {
      $row[] = form_render($form[$key]['description']) . form_render($form[$key]['screenshot']);
      $row[] = array('data' => form_render($form['theme'][$key]), 'align' => 'center');
    }
    $rows[] = $row;
  }

  $header = array(t('Name/Screenshot'), t('Selected'));
  $output = theme('table', $header, $rows);
  return $output;
}
?>
